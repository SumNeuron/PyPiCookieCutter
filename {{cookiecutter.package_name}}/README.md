# {{cookiecutter.package_name}}
![License][License Badge]
![Version][Version Badge]






[License Badge]: https://img.shields.io/badge/license-{{cookiecutter.license}}-lightgrey.svg

[Version Badge]: https://img.shields.io/badge/version-{{cookiecutter.version}}-blue.svg
