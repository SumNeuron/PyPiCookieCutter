import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="{{cookiecutter.package_name}}",
    version="{{cookiecutter.version}}",
    author="{{cookiecutter.package_name}}",
    author_email="{{cookiecutter.email}}",
    description="Python Package {{cookiecutter.package_name}}",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/SumNeuron/{{cookiecutter.package_name}}",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: {{cookiecutter.license}}",
        "Operating System :: OS Independent",
    ],
)
